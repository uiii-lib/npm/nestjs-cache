import fs from 'fs';
import path from 'path';
import mkdirp from 'mkdirp';

import { Injectable } from "@nestjs/common";

import { CacheConfig } from './cache.config';

@Injectable()
export class CacheService {
	constructor(private config: CacheConfig) {}

	readCache(namespace: string, key: string, encoding?: BufferEncoding) {
		const cacheFile = this.getCacheFile(namespace, key);

		if (!fs.existsSync(cacheFile)) {
			return null;
		}

		return fs.readFileSync(cacheFile, encoding);
	}

	writeCache(namespace: string, key: string, data: any) {
		mkdirp.sync(path.join(this.config.cacheDir, namespace));

		return fs.writeFileSync(this.getCacheFile(namespace, key), data);
	}

	protected getCacheFile(namespace: string, key: string) {
		const filename = key.toLowerCase()
			.replace(/[^a-zA-Z0-9_\-]+/g, '_');

		return path.join(this.config.cacheDir, namespace, filename);
	}
}
