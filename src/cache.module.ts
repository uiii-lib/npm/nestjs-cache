import { Module } from "@nestjs/common";
import { ConfigModule } from "@uiii-lib/nestjs-config";
import { CacheConfig } from "./cache.config";

import { CacheService } from "./cache.service";

@Module({
	imports: [
		ConfigModule
	],
	providers: [
		CacheConfig,
		CacheService
	],
	exports: [
		CacheService
	]
})
export class CacheModule {}
