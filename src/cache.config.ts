import { Injectable } from '@nestjs/common';
import { ConfigService } from '@uiii-lib/nestjs-config';

@Injectable()
export class CacheConfig {
	constructor(private config: ConfigService) {}

	get cacheDir(): string {
		return this.config.get('CACHE_DIR', true)!;
	}
}
