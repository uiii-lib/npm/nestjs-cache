import fs from 'fs';
import path from 'path';

import { ConfigService } from '@uiii-lib/nestjs-config';

import { CacheConfig } from '../src/cache.config';
import { CacheService } from '../src/cache.service';

jest.mock('fs');

describe("Cache service", () => {
	let cacheConfig: CacheConfig;
	let cacheService: CacheService;

	beforeEach(() => {
		console.log("clear");
		(fs.readFileSync as any).mockReset();
		(fs.writeFileSync as any).mockReset();

		process.env.STATIC_PATH = "";
		process.env.CACHE_DIR = "/path/to/cache";
		const configService = new ConfigService();

		(fs.existsSync as any).mockReturnValue(true);

		cacheConfig = new CacheConfig(configService);
		cacheService = new CacheService(cacheConfig);
	})

	describe("writeCache", () => {
		it("should write test data to a file on disk", () => {
			const data = "text data";
			cacheService.writeCache("namespace", "key", data);

			expect(fs.writeFileSync).toHaveBeenCalledWith(path.join(cacheConfig.cacheDir, 'namespace', 'key'), data);
		});

		it("should write binary data to a file on disk", () => {
			const data = Buffer.from([1,2,3,4,5]);
			cacheService.writeCache("namespace", "key", data);

			expect(fs.writeFileSync).toHaveBeenCalledWith(path.join(cacheConfig.cacheDir, 'namespace', 'key'), data);
		});

		it("should use safe characters only in cache file name", () => {
			const data = "text data";
			cacheService.writeCache("namespace", "a/b/c:d@e+f**g_h-09", data);

			expect(fs.writeFileSync).toHaveBeenCalledWith(path.join(cacheConfig.cacheDir, 'namespace', 'a_b_c_d_e_f_g_h-09'), data);
		})
	});

	describe("readCache", () => {
		it("should read text data from a file on disk", () => {
			const data = "text data";

			(fs.readFileSync as any).mockImplementation((filePath: string, encoding?: string) =>
				filePath === path.join(cacheConfig.cacheDir, 'namespace', 'key') && encoding === 'utf-8'
					? data
					: ''
			);

			const cacheData = cacheService.readCache("namespace", "key", "utf-8");

			expect(cacheData).toBe(data);
		});

		it("should read binary data from a file on disk", () => {
			const data = Buffer.from([1,2,3,4,5]);

			(fs.readFileSync as any).mockImplementation((filePath: string, encoding?: string) =>
				filePath === path.join(cacheConfig.cacheDir, 'namespace', 'key') && encoding === undefined
					? data
					: ''
			);

			const cacheData = cacheService.readCache("namespace", "key", undefined);

			expect(cacheData).toBe(data);
		});

		it("it should return null if there is no cache with the key", () => {
			(fs.existsSync as any).mockReturnValue(false);
			const cacheData = cacheService.readCache("namespace", "key");

			expect(cacheData).toBe(null);

		})
	});
})
